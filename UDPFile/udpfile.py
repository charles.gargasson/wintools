import socket
import os

# Server configuration
UDP_IP = "0.0.0.0"  # Listen on all interfaces
UDP_PORT = 6444     # Port to listen on
BUFFER_SIZE = 1024  # Size of data chunks

# Create the UDP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind((UDP_IP, UDP_PORT))

print(f"[*] UDP File Server listening on {UDP_IP}:{UDP_PORT}")

while True:
    # Receive request from client
    data, addr = sock.recvfrom(1024)
    filename = data.decode().strip()
    print(f"[+] Sending '{filename}' to {addr} ", end='')
    if os.path.exists(filename) and os.path.isfile(filename):
        # Send file in chunks
        with open(filename, "rb") as f:
            while chunk := f.read(BUFFER_SIZE):
                sock.sendto(chunk, addr)
                _, addr = sock.recvfrom(1024)
        
        # Send termination message
        sock.sendto(b"UDPEOF", addr)
        print(f"\n[+] File '{filename}' sent successfully to {addr}")
    else:
        sock.sendto(b"UDPEOF", addr)

