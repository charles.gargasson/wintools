#######################
CreateProcessWithLogonW
#######################

*****
Build
*****

|

.. code-block:: bash

   bash dockerbuild.sh

|

*****
Usage
*****

.. code-block:: bash

   ./CreateProcessWithLogonW.exe -u user -d domain.com -p 'password' -c "whoami.exe /all"

|

**********
LOGON LIST
**********

.. code-block:: 

   List of Common Logon Types:
   Interactive (LOGON32_LOGON_INTERACTIVE):

   Value: 2
   Used for interactive logons at the console.
   Batch (LOGON32_LOGON_BATCH):

   Value: 4
   Used by batch servers or scheduled tasks.
   Service (LOGON32_LOGON_SERVICE):

   Value: 5
   Used by services and system processes.
   Network (LOGON32_LOGON_NETWORK):

   Value: 3
   Used for accessing resources over the network.
   Network Cleartext (LOGON32_LOGON_NETWORK_CLEARTEXT):

   Value: 8
   Used for accessing resources over the network with credentials transmitted in plaintext (less secure).
   New Credentials (LOGON32_LOGON_NEW_CREDENTIALS):

   Value: 9
   Allows obtaining new credentials on behalf of the user.
   Cached Interactive (LOGON32_LOGON_CACHED_INTERACTIVE):

   Value: 7
   Used for cached logons when a network is not available.