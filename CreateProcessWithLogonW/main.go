package main

import (
	"bytes"
	"flag"
	"fmt"
	"os"
	"syscall"
	"unsafe"
)

var (
	advapi32                    = syscall.NewLazyDLL("advapi32.dll")
	procCreateProcessWithLogonW = advapi32.NewProc("CreateProcessWithLogonW")
	procLogonUserW              = advapi32.NewProc("LogonUserW")
	procCreateProcessAsUserW    = advapi32.NewProc("CreateProcessAsUserW")
)

const (
	LOGON32_LOGON_INTERACTIVE        uint32 = 2
	LOGON32_LOGON_NETWORK            uint32 = 3
	LOGON32_LOGON_BATCH              uint32 = 4
	LOGON32_LOGON_SERVICE            uint32 = 5
	LOGON32_LOGON_UNLOCK             uint32 = 7
	LOGON32_LOGON_NETWORK_CLEARTEXT  uint32 = 8
	LOGON32_LOGON_NEW_CREDENTIALS    uint32 = 9
	LOGON32_LOGON_CACHED_INTERACTIVE uint32 = 10
	LOGON32_LOGON_REMOTE_INTERACTIVE uint32 = 11

	LOGON32_PROVIDER_DEFAULT uint32 = 0
	LOGON32_PROVIDER_WINNT35 uint32 = 1
	LOGON32_PROVIDER_WINNT40 uint32 = 2
	LOGON32_PROVIDER_WINNT50 uint32 = 3

	ERROR_LOGON_FAILURE syscall.Errno = 1326

	CREATE_UNICODE_ENVIRONMENT = 0x00000400
	CREATE_NO_WINDOW           = 0x08000000
	CREATE_NEW_CONSOLE         = 0x00000010
	DETACHED_PROCESS           = 0x00000008

	LOGON_NETCREDENTIALS_ONLY = 0x00000002
	LOGON_WITH_PROFILE        = 0x00000001
)

var logonTypes = map[uint32]string{
	LOGON32_LOGON_INTERACTIVE:        "Interactive",
	LOGON32_LOGON_BATCH:              "Batch",
	LOGON32_LOGON_SERVICE:            "Service",
	LOGON32_LOGON_NETWORK:            "Network",
	LOGON32_LOGON_NETWORK_CLEARTEXT:  "Network Clear Text",
	LOGON32_LOGON_NEW_CREDENTIALS:    "New Credentials",
	LOGON32_LOGON_CACHED_INTERACTIVE: "Cached Interactive",
	LOGON32_LOGON_REMOTE_INTERACTIVE: "Remote Interactive",
	LOGON32_LOGON_UNLOCK:             "Unlock",
}

var logonTypesorder = []uint32{
	LOGON32_LOGON_INTERACTIVE,
	LOGON32_LOGON_NETWORK,
	LOGON32_LOGON_BATCH,
	LOGON32_LOGON_SERVICE,
	LOGON32_LOGON_NETWORK_CLEARTEXT,
	LOGON32_LOGON_NEW_CREDENTIALS,
	LOGON32_LOGON_CACHED_INTERACTIVE,
	LOGON32_LOGON_REMOTE_INTERACTIVE,
	LOGON32_LOGON_UNLOCK,
}

func main() {
	username := flag.String("u", "Administrator", "username")
	password := flag.String("p", "", "password")
	domain := flag.String("d", "", "domain")
	application := flag.String("f", ``, "file")
	commandLine := flag.String("c", ``, "args")
	workingDir := flag.String("w", ``, "workingdir")
	method := flag.Int("m", 0, "method 0:CreateProcessWithLogon 1:CreateProcessAsUser")

	flag.Parse()

	fmt.Println("\nPARAMETERS")
	fmt.Printf("  User : %s\n", *username)
	fmt.Printf("  Domain : %s\n", *domain)
	fmt.Printf("  Password : %s\n", *password)
	fmt.Printf("  Application : %s\n", *application)
	fmt.Printf("  Commandline : %s\n", *commandLine)

	fmt.Println("\nMETHOD : CreateProcessWithLogon")
	valid, logonType, token := GetLogonToken(*username, *domain, *password)
	if valid == false {
		fmt.Println("Error: No valid login method found")
		return
	}

	if *method == 0 {
		fmt.Println("\nMETHOD : CreateProcessWithLogon")
		err := CreateProcessWithLogon(logonType, *workingDir, *username, *domain, *password, *application, *commandLine)
		if err != nil {
			fmt.Println("Error:", err)
		}
	}

	if *method == 1 {
		err := CreateProcessAsUser(token, *workingDir, *application, *commandLine)
		if err != nil {
			fmt.Println("Error:", err)
		}
	}
}

func StrToPTR(strvar string) uintptr {
	var ptr_var uintptr = 0
	if len(strvar) > 0 {
		uint16_var, _ := syscall.UTF16PtrFromString(strvar)
		ptr_var = uintptr(unsafe.Pointer(uint16_var))
	}
	return ptr_var
}

func GetLogonToken(username, domain, password string) (bool, uint32, syscall.Handle) {
	var ptr_password uintptr = 0
	if len(password) > 0 {
		uint16_password, _ := syscall.UTF16PtrFromString(password)
		ptr_password = uintptr(unsafe.Pointer(uint16_password))
	}

	var ptr_domain uintptr = 0
	if len(domain) > 0 {
		uint16_domain, _ := syscall.UTF16PtrFromString(domain)
		ptr_domain = uintptr(unsafe.Pointer(uint16_domain))
	}

	for _, logonType := range logonTypesorder {
		logonTypeTxt := logonTypes[logonType]
		var token syscall.Handle
		ret, _, err := procLogonUserW.Call(
			StrToPTR(username),
			ptr_domain,   // Domain (empty for local : uintptr(0)
			ptr_password, // Password (empty for no password)
			uintptr(logonType),
			uintptr(LOGON32_PROVIDER_DEFAULT),
			uintptr(unsafe.Pointer(&token)),
		)
		if ret == 0 {
			if err.(syscall.Errno) == ERROR_LOGON_FAILURE {
				// fmt.Printf("  -- FAILED %d (%s)\n", logonType, logonTypeTxt)
			} else {
				//fmt.Printf("  -- FAILED %d (%s) %v\n", logonType, logonTypeTxt, err)
			}
			continue
		}
		fmt.Printf("!! SUCCESS %d (%s)\n", logonType, logonTypeTxt)
		return true, logonType, token
	}
	var token syscall.Handle
	return false, 0, token
}

func CreateProcessAsUser(token syscall.Handle, workingDir, application, commandLine string) error {
	var ptr_application uintptr = 0
	if len(application) > 0 {
		uint16_application, _ := syscall.UTF16PtrFromString(application)
		ptr_application = uintptr(unsafe.Pointer(uint16_application))
	}

	var ptr_commandLine uintptr = 0
	if len(commandLine) > 0 {
		uint16_commandLine, _ := syscall.UTF16PtrFromString(commandLine)
		ptr_commandLine = uintptr(unsafe.Pointer(uint16_commandLine))
	}

	var ptr_workingDir uintptr = 0
	if len(workingDir) > 0 {
		uint16_workingDir, _ := syscall.UTF16PtrFromString(workingDir)
		ptr_workingDir = uintptr(unsafe.Pointer(uint16_workingDir))
	}

	defer syscall.CloseHandle(token)

	var startupInfo syscall.StartupInfo
	var processInfo syscall.ProcessInformation
	startupInfo.Cb = uint32(unsafe.Sizeof(startupInfo))
	var envInfo syscall.Handle
	creationFlags := CREATE_UNICODE_ENVIRONMENT | CREATE_NEW_CONSOLE // ex: CREATE_UNICODE_ENVIRONMENT | CREATE_NEW_CONSOLE

	ret, _, err := procCreateProcessAsUserW.Call(
		uintptr(token),                        // hToken
		ptr_application,                       //appPath
		ptr_commandLine,                       //cmdline
		0,                                     // lpProcessAttributes
		0,                                     // lpThreadAttributes
		0,                                     // bInheritHandles
		uintptr(creationFlags),                // dwCreationFlags
		uintptr(envInfo),                      // lpEnvironment
		ptr_workingDir,                        // lpCurrentDirectory
		uintptr(unsafe.Pointer(&startupInfo)), // lpStartupInfo
		uintptr(unsafe.Pointer(&processInfo)), // lpProcessInformation
	)

	if ret == 0 {
		return err
	}
	defer syscall.CloseHandle(processInfo.Thread)
	defer syscall.CloseHandle(processInfo.Process)
	fmt.Println("Process created successfully with PID:", processInfo.ProcessId)
	return nil
}

func CreateProcessWithLogon(logonType uint32, workingDir, username, domain, password, application, commandLine string) error {
	ptr_username, _ := syscall.UTF16PtrFromString(username)
	ptr_password, _ := syscall.UTF16PtrFromString(password)

	var ptr_domain uintptr = 0
	if len(domain) > 0 {
		uint16_domain, _ := syscall.UTF16PtrFromString(domain)
		ptr_domain = uintptr(unsafe.Pointer(uint16_domain))
	}

	var ptr_application uintptr = 0
	if len(application) > 0 {
		uint16_application, _ := syscall.UTF16PtrFromString(application)
		ptr_application = uintptr(unsafe.Pointer(uint16_application))
	}

	var ptr_commandLine uintptr = 0
	if len(commandLine) > 0 {
		uint16_commandLine, _ := syscall.UTF16PtrFromString(commandLine)
		ptr_commandLine = uintptr(unsafe.Pointer(uint16_commandLine))
	}

	var ptr_workingDir uintptr = 0
	if len(workingDir) > 0 {
		uint16_workingDir, _ := syscall.UTF16PtrFromString(workingDir)
		ptr_workingDir = uintptr(unsafe.Pointer(uint16_workingDir))
	}

	var LogonFlags uintptr = 0
	//if logonType == LOGON32_LOGON_NEW_CREDENTIALS {
	//	LogonFlags = LOGON_NETCREDENTIALS_ONLY
	//}

	var startupInfo syscall.StartupInfo
	var processInfo syscall.ProcessInformation
	startupInfo.Cb = uint32(unsafe.Sizeof(startupInfo))
	var envInfo syscall.Handle
	creationFlags := CREATE_NO_WINDOW

	// Create pipes for stdout and stderr
	stdoutRead, stdoutWrite, err := os.Pipe()
	if err != nil {
		return err
	}
	defer stdoutRead.Close()
	defer stdoutWrite.Close()

	stderrRead, stderrWrite, err := os.Pipe()
	if err != nil {
		return err
	}
	defer stderrRead.Close()
	defer stderrWrite.Close()

	// Set up startup info
	startupInfo.Cb = uint32(unsafe.Sizeof(startupInfo))
	startupInfo.Flags = syscall.STARTF_USESTDHANDLES
	startupInfo.StdOutput = syscall.Handle(stdoutWrite.Fd())
	startupInfo.StdErr = syscall.Handle(stderrWrite.Fd())

	_, _, err = procCreateProcessWithLogonW.Call(
		uintptr(unsafe.Pointer(ptr_username)),
		ptr_domain,
		uintptr(unsafe.Pointer(ptr_password)),
		LogonFlags,
		ptr_application,
		ptr_commandLine,
		uintptr(creationFlags), // CreationFlags, set to 0
		uintptr(envInfo),       // lpEnvironment, set to 0
		ptr_workingDir,         // lpCurrentDirectory, set to empty string
		uintptr(unsafe.Pointer(&startupInfo)),
		uintptr(unsafe.Pointer(&processInfo)),
	)

	if err != nil && err.Error() != "The operation completed successfully." {
		return err
	}
	fmt.Println("Process created successfully with PID:", processInfo.ProcessId)

	// Close the write ends of the pipes in the parent process
	stdoutWrite.Close()
	stderrWrite.Close()

	// Read from stdout and stderr
	var stdoutBuf, stderrBuf bytes.Buffer
	stdoutDone := make(chan error)
	stderrDone := make(chan error)

	go func() {
		_, err := stdoutBuf.ReadFrom(stdoutRead)
		stdoutDone <- err
	}()

	go func() {
		_, err := stderrBuf.ReadFrom(stderrRead)
		stderrDone <- err
	}()

	defer syscall.CloseHandle(processInfo.Process)
	defer syscall.CloseHandle(processInfo.Thread)
	syscall.WaitForSingleObject(processInfo.Process, syscall.INFINITE)

	// Wait for stdout and stderr reading to finish
	if err := <-stdoutDone; err != nil {
		return err
	}
	if err := <-stderrDone; err != nil {
		return err
	}

	fmt.Println("stdout:\n", stdoutBuf.String())
	fmt.Println("stderr:\n", stderrBuf.String())

	return nil
}

////////
