#!/bin/bash
cd -- "$(dirname -- "$0")"
docker build -t gobuilder:latest .
docker run --rm -it -v "$(pwd):/workspace/" gobuilder:latest bash /workspace/build.sh
