require 'winrm'
require 'getoptlong'

# https://medium.com/r3d-buck3t/certificate-based-authentication-over-winrm-13197265c790
# ruby winrmcert.rb --ip 10.129.227.113 --cert user.crt --key user.key 

opts = GetoptLong.new(
    [ '--help', '-h', GetoptLong::NO_ARGUMENT ],
    [ '--ip', '-i', GetoptLong::REQUIRED_ARGUMENT ],
    [ '--cert', '-c', GetoptLong::REQUIRED_ARGUMENT ],
    [ '--key', '-k', GetoptLong::REQUIRED_ARGUMENT ]
)

ip=""
cert=""
key=""
opts.each do |opt, arg|
  case opt
    when '--help'
      puts <<-EOF
-h, --help:
  show help

-i, --ip:
  IP of server

-c, --cert:
  Certificate file

-k, --key:
  Certificate key
      EOF
    when '--ip'
      ip = arg
    when '--cert'
      cert = arg
    when '--key'
      key = arg
  end
end

#Append necessary changes in winrm_shell.rb
conn = WinRM::Connection.new(
  endpoint:  "https://#{ip}:5986/wsman",
  transport: :ssl,
  :client_cert => cert,
  :client_key => key,
  :no_ssl_peer_verification => true)

command=""

conn.shell(:powershell) do |shell|
    until command == "exit\n" do
        output = shell.run("-join($id,'PS ',$(whoami),'@',$env:computername,' ',$((gi $pwd).Name),'> ')")
        print(output.output.chomp)
        command = gets        
        output = shell.run(command) do |stdout, stderr|
            STDOUT.print stdout
            STDERR.print stderr
        end
    end    
    puts "Exiting with code #{output.exitcode}"
end