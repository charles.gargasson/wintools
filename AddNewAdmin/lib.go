//go:build lib
// +build lib

package main

import "C"

// Cgo is required to compile a dll

func init() {
	_ = C.CString("")
	main()
}

//export Start
func Start() {}

// Example for dnsadmin exploitation
//
// //export DnsPluginInitialize
// func DnsPluginInitialize() {}
//
// //export DnsPluginQuery
// func DnsPluginQuery() {}
//
// //export DnsPluginCleanup
// func DnsPluginCleanup() {}
