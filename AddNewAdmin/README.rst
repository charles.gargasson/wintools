###########
AddNewAdmin
###########

| x32 and x64 EXE/DLL payloads that add a new user into administrators group using SID value 'S-1-5-32-544'
| 
| USER : hackerbeepboop
| PASS : Blabliblou_1
| 
| Feel free to modify user credentials or SID values (it's a list)

|

*****
Build
*****

|

| Build with docker

.. code-block:: bash

   bash dockerbuild.sh

|

| Build without docker

.. code-block:: bash

   bash build.sh

|

| Generated payloads

.. code-block:: 

   # AddNewAdmin64.exe
   # AddNewAdmin32.exe
   # AddNewAdmin64.dll
   # AddNewAdmin32.dll

|
