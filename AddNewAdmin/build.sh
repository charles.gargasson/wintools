#!/bin/bash
cd -- "$(dirname -- "$0")"
env GOOS=windows GOARCH=amd64 CGO_ENABLED=0 CC=x86_64-w64-mingw32-gcc go build -o AddNewAdmin64.exe
env GOOS=windows GOARCH=386 CGO_ENABLED=0 CC=x86_64-w64-mingw32-gcc go build -o AddNewAdmin32.exe
env GOOS=windows GOARCH=amd64 CGO_ENABLED=1 CC=x86_64-w64-mingw32-gcc go build -tags="lib" -o AddNewAdmin64.dll -buildmode=c-shared
env GOOS=windows GOARCH=386 CGO_ENABLED=1 CC=i686-w64-mingw32-gcc go build -tags="lib" -o AddNewAdmin32.dll -buildmode=c-shared
